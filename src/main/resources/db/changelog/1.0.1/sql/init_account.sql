CREATE TABLE accounts
(
    account_id SERIAL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    role VARCHAR NOT NULL,
    active BOOLEAN NOT NULL,
    PRIMARY KEY (account_id),
    UNIQUE (username)
);

 INSERT INTO accounts (username, password, role, active) VALUES ('admin', 'admin', 'ROLE_ADMIN', true);