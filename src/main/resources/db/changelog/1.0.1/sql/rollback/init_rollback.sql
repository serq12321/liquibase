drop TABLE if exists people;

drop sequence if exists people_person_id_seq;

drop TABLE if exists employments;

drop sequence if exists employments_employment_id_seq;