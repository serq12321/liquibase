CREATE TABLE people
(
    person_id   SERIAL       NOT NULL,
    first_name  VARCHAR(256) NOT NULL,
    last_name   VARCHAR(256) NOT NULL,
    middle_name VARCHAR(256) NULL,
    birth_date  DATE         NOT NULL,
    gender      VARCHAR(6)   NOT NULL,
    PRIMARY KEY (person_id)
);

comment on table people is 'Инфорамция о человеке';

comment on column people.person_id is 'идентификатор.';

comment on column people.first_name is 'Имя';

comment on column people.last_name is 'Фамилия';

comment on column people.middle_name is 'Фамилия';

comment on column people.birth_date is 'Дата рождения';

comment on column people.gender is 'Пол';


CREATE TABLE employments
(
    employment_id        SERIAL       NOT NULL,
    version              INTEGER      NULL,
    start_date           DATE         NOT NULL,
    end_date             DATE         NULL,
    work_type_id         BIGINT       NULL,
    organization_name    VARCHAR(256) NOT NULL,
    organization_address TEXT         NOT NULL,
    position_name        VARCHAR(256) NOT NULL,
    person_id            BIGINT       NOT NULL,
    PRIMARY KEY (employment_id),
    FOREIGN KEY (person_id) REFERENCES people (person_id)
);

comment on table employments is 'запись о трудовой деятельности.';

comment on column employments.employment_id is 'идентификатор.';

comment on column employments.version is 'реализация оптимистической блокировки';

comment on column employments.start_date is 'дата начала трудовой деятельности';

comment on column employments.end_date is 'дата окончания трудовой деятельности';

comment on column employments.work_type_id is 'тип деятельности';

comment on column employments.organization_name is 'наименование организации.';

comment on column employments.organization_address is 'адрес организации.';

comment on column employments.position_name is 'должность.';
