package com.example.controller;

import com.example.api.AccountService;
import com.example.dto.AccountDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("accounts")
public class AccountController {

    AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("save")
    public void save(@RequestBody @Validated AccountDTO accountDTO) {
        accountService.save(accountDTO);
    }

    @GetMapping("{username}")
    public AccountDTO getById(@PathVariable String username) {
        return accountService.getByUsername(username);
    }

    @GetMapping("getAll")
    public List<AccountDTO> getAll() {
        return accountService.getAll();
    }
}
