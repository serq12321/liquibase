package com.example.controller;

import com.example.api.EmploymentService;
import com.example.dto.EmploymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employments")
public class EmploymentController {

    @Autowired
    EmploymentService employmentService;

    @PostMapping("updatePerson/{id}")
    void updateAll(@RequestBody @Validated List<EmploymentDTO> listOfEmploymentsDTO, @PathVariable long id) {
        employmentService.updateAllWithPersonId(listOfEmploymentsDTO, id);
    }
}
