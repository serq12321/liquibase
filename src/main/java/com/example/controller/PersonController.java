package com.example.controller;

import com.example.api.PersonService;
import com.example.dto.PersonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("people")
public class PersonController {

    @Autowired
    PersonService personService;

    @PostMapping("save")
    public void save(@RequestBody @Validated PersonDTO personDTO) {
        personService.save(personDTO);
    }
}
