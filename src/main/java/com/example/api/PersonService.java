package com.example.api;

import com.example.dto.PersonDTO;

public interface PersonService {
    void save(PersonDTO personDTO);
}
