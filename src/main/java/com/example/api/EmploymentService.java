package com.example.api;

import com.example.dto.EmploymentDTO;

import java.util.List;

public interface EmploymentService {
    void updateAllWithPersonId(List<EmploymentDTO> listOfEmploymentsFromFront, long id);
}
