package com.example.api;

import com.example.dto.AccountDTO;
import java.util.List;

public interface AccountService {

    void save(AccountDTO accountDTO);

    AccountDTO getByUsername(String username);

    List<AccountDTO> getAll();
}
