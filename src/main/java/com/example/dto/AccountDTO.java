package com.example.dto;

import com.sun.istack.internal.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {

    private long accountId;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String role;

    @NotNull
    private boolean active;
}