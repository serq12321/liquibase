package com.example.dto;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmploymentDTO {
    private long employmentId;

    @Nullable
    private int version;

    @NotNull
    private LocalDate startDate;

    @Nullable
    private LocalDate endDate;

    @Nullable
    private long workTypeId;

    @NotNull
    private String organizationName;

    @NotNull
    private String organizationAddress;

    @NotNull
    private String positionName;

    @NotNull
    private long personId;
}