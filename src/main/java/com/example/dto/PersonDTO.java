package com.example.dto;

import com.example.enums.Gender;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonDTO {
    private long personId;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private LocalDate birthDate;

    @NotNull
    private Gender gender;
}
