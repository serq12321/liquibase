package com.example.repository;

import com.example.model.PersonEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PersonRepository {

    @Insert("INSERT INTO people (first_name, last_name, middle_name, birth_date, gender) " +
            "VALUES (#{firstName}, #{lastName}, #{middleName}, #{birthDate}, #{gender})")
    void save(PersonEntity personEntity);
}
