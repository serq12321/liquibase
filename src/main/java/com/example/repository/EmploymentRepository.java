package com.example.repository;

import com.example.model.EmploymentEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EmploymentRepository {

    @Select("SELECT * FROM employments WHERE person_id = #{id}")
    List<EmploymentEntity> getAllWithPersonId(long id);

    @Select("SELECT * FROM employments WHERE employment_id = #{id}")
    EmploymentEntity getById(long id);

    @Insert("INSERT INTO employments (version, start_date, end_date, work_type_id, " +
            "organization_name, organization_address, position_name, person_id) " +
            "VALUES (#{version}, #{startDate}, #{endDate}, #{workTypeId}, " +
            "#{organizationName}, #{organizationAddress}, #{positionName}, #{personId})")
    void save(EmploymentEntity employmentEntity);

    @Update("UPDATE employments " +
            "SET version = #{version}, " +
            "start_date = #{startDate}, " +
            "end_date = #{endDate}, " +
            "work_type_id = #{workTypeId}, " +
            "organization_name = #{organizationName}, " +
            "organization_address = #{organizationAddress}, " +
            "position_name = #{positionName}, " +
            "person_id = #{personId} " +
            "WHERE employment_id = #{employmentId}")
    void updateById(EmploymentEntity employmentEntity);

    @Delete("DELETE FROM employments WHERE employment_id = #{id}")
    void deleteById(long id);
}
