package com.example.repository;

import com.example.model.AccountEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AccountRepository {

    @Insert("INSERT INTO accounts (username, password, role, active) " +
            "VALUES (#{username}, #{password}, #{role}, #{active})")
    void save(AccountEntity accountEntity);

    @Select("SELECT * FROM accounts WHERE username = #{username}")
    AccountEntity getByUsername(String username);

    @Select("SELECT * FROM accounts")
    List<AccountEntity> getAll();
}
