package com.example.model;

import com.example.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonEntity {
    private long personId;

    private String firstName;

    private String lastName;

    private String middleName;

    private LocalDate birthDate;

    private Gender gender;
}