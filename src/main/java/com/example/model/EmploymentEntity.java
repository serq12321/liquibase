package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmploymentEntity {
    private long employmentId;

    private int version;

    private LocalDate startDate;

    private LocalDate endDate;

    private long workTypeId;

    private String organizationName;

    private String organizationAddress;

    private String positionName;

    private long personId;
}
