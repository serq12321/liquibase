package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity {

    private long accountId;

    private String username;

    private String password;

    private String role;

    private boolean active;
}