package com.example.service;

import com.example.api.AccountService;
import com.example.dto.AccountDTO;
import com.example.model.AccountEntity;
import com.example.repository.AccountRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    ModelMapper modelMapper;
    AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(ModelMapper modelMapper, AccountRepository accountRepository) {
        this.modelMapper = modelMapper;
        this.accountRepository = accountRepository;
    }

    @Override
    public void save(AccountDTO accountDTO) {
        accountRepository.save(modelMapper.map(accountDTO, AccountEntity.class));
    }

    @Override
    public AccountDTO getByUsername(String username) {
        return modelMapper.map(accountRepository.getByUsername(username), AccountDTO.class);
    }

    @Override
    public List<AccountDTO> getAll() {
        return accountRepository.getAll().stream()
                .map(accountEntity -> modelMapper.map(accountEntity, AccountDTO.class))
                .collect(Collectors.toList());
    }
}
