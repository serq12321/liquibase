package com.example.service;

import com.example.api.PersonService;
import com.example.repository.PersonRepository;
import com.example.dto.PersonDTO;
import com.example.model.PersonEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerssonServiceImpl implements PersonService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PersonRepository personRepository;

    public void save(PersonDTO personDTO) {
        PersonEntity personEntity = modelMapper.map(personDTO, PersonEntity.class);
        personRepository.save(personEntity);
    }
}
