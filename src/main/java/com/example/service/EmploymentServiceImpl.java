package com.example.service;

import com.example.api.EmploymentService;
import com.example.repository.EmploymentRepository;
import com.example.dto.EmploymentDTO;
import com.example.model.EmploymentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    private List<EmploymentDTO> listOfEmploymentsFromFrontWithPersonId;
    private List<EmploymentDTO> listOfEmploymentsFromDBWithPersonId;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    EmploymentRepository employmentRepository;

    @Override
    public void updateAllWithPersonId(List<EmploymentDTO> listOfEmploymentsFromFront, long id) {

        listOfEmploymentsFromFrontWithPersonId = getAllFromFrontWithPersonId(listOfEmploymentsFromFront, id);

        listOfEmploymentsFromDBWithPersonId = getAllFromDBWithPersonId(id);

        deleteElementsFromDBWhereNecessary();

        selectAndUpdateElementsInDBWhereNecessary();
    }

    private List<EmploymentDTO> getAllFromFrontWithPersonId(List<EmploymentDTO> listOfEmploymentsFromFront, long id) {
        return listOfEmploymentsFromFront.stream()
                .filter(employmentDTO -> employmentDTO.getPersonId() == id)
                .collect(Collectors.toList());
    }

    private List<EmploymentDTO> getAllFromDBWithPersonId(long id) {
        List<EmploymentEntity> entities = employmentRepository.getAllWithPersonId(id);
        return entities.stream().
                map(employmentEntity -> modelMapper.map(employmentEntity, EmploymentDTO.class))
                .collect(Collectors.toList());
    }

    private void deleteElementsFromDBWhereNecessary() {
        listOfEmploymentsFromDBWithPersonId.stream()
                .filter(employmentDTO -> !doesFrontHaveId(employmentDTO.getEmploymentId()))
                .forEach(employmentDTO -> employmentRepository.deleteById(employmentDTO.getEmploymentId()));
    }

    private boolean doesFrontHaveId(long id) {
        return (listOfEmploymentsFromFrontWithPersonId.stream()
                .filter(employmentDTO -> employmentDTO.getEmploymentId() == id)
                .count() == 1);
    }

    private void selectAndUpdateElementsInDBWhereNecessary() {
        for (EmploymentDTO employmentFromFront: listOfEmploymentsFromFrontWithPersonId) {
            if (employmentFromFront.getEmploymentId() == 0) {
                saveEmployment(employmentFromFront);
            } else if (doesDBHaveElementWithSameIdAndAnotherBody(employmentFromFront)) {
                updateEmployment(employmentFromFront);
            }
        }
    }

    private void saveEmployment(EmploymentDTO employmentFromFront) {
        employmentRepository.save(modelMapper.map(employmentFromFront, EmploymentEntity.class));
    }

    private boolean doesDBHaveElementWithSameIdAndAnotherBody(EmploymentDTO employmentFromFront) {
        EmploymentEntity entity = employmentRepository.getById(employmentFromFront.getEmploymentId());
        EmploymentDTO employmentFromDB = modelMapper.map(entity, EmploymentDTO.class);
        return !employmentFromDB.equals(employmentFromFront);
    }

    private void updateEmployment(EmploymentDTO employmentFromFront) {
        employmentRepository.updateById(modelMapper.map(employmentFromFront, EmploymentEntity.class));
    }
}
